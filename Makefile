all:
	dune build @all

doc:
	dune build @doc

install: all
	dune build @install
	dune install

clean:
	dune clean

tests: all
	dune build @install
	dune runtest -f

.PHONY: all doc install clean tests
