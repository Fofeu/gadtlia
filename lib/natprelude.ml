type 'a s = 'a Nat.s
type z = Nat.z

module Ops =
struct
  type ('a,'b,'c) add = ('a,'b,'c) Nat.Add.add
  let add = Nat.Add.add
  type ('a,'b,'c) sub = ('a,'b,'c) Nat.Sub.sub
  let sub = Nat.Sub.sub
  type ('a,'b,'c) mul = ('a,'b,'c) Nat.Mul.mul
  let mul = Nat.Mul.mul
  type ('a,'b,'c,'d) div = ('a,'b,'c,'d) Nat.Div.div
  let div = Nat.Div.div

  let cmp = Nat.Cmp.cmp
  type ('a,'b) eq = ('a,'b) Nat.Cmp.eq
  type ('a,'b) lt = ('a,'b) Nat.Cmp.lt
  type ('a,'b) gt = ('a,'b) Nat.Cmp.gt
  type ('a,'b) le = ('a,'b) Nat.Cmp.le
  type ('a,'b) ge = ('a,'b) Nat.Cmp.ge
end
