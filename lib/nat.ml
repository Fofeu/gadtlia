(* Natural numbers *)

type z = Z_
type 'a s = S_ of 'a

type _ t =
  | Z: z t
  | S: 'n t -> 'n s t

module Cmp =
struct

  type (_,_) lt = Lt: ('a,'b s) lt_data * ('a,'b s) lt_proof -> ('a,'b s) lt

  and ('a,'b) lt_data = {lhs: 'a t; rhs: 'b t}

  and (_,_) lt_proof =
    | Lt0: (z,'b s) lt_proof
    | LtS: ('a,'b) lt_proof -> ('a s,'b s) lt_proof

  type (_,_) eq = Eq: ('a,'b) eq_data * ('a,'b) eq_proof -> ('a,'a) eq

  and ('a,'b) eq_data = {lhs: 'a t; rhs: 'b t}

  and (_,_) eq_proof = EqP: ('a,'a) eq_proof

  type (_,_) gt = Gt: ('a,'b) gt_data * ('a,'b) gt_proof -> ('a,'b) gt

  and ('a,'b) gt_data = {lhs: 'a t; rhs: 'b t}

  and (_,_) gt_proof =
    | Gt0: ('a s,z) gt_proof
    | GtS: ('a,'b) gt_proof -> ('a s,'b s) gt_proof

  type ('a,'b) le = (('a,'b) eq, ('a,'b) lt) Either.t
  type ('a,'b) ge = (('a,'b) eq, ('a,'b) gt) Either.t

  type (_,_) ecmp =
    | ELt: ('a,'b) lt -> ('a,'b) ecmp
    | EEq: ('a,'b) eq -> ('a,'b) ecmp
    | EGt: ('a,'b) gt -> ('a,'b) ecmp

  type is_lt = Lt_
  type is_eq = Eq_
  type is_gt = Gt_

  let rec cmp: type a b. a t -> b t -> (a,b) ecmp =
    fun lhs rhs ->
    match lhs,rhs with
    | (Z as lhs), ((S _) as rhs) -> ELt (Lt ({lhs;rhs}, Lt0))
    | (Z as lhs), (Z as rhs) -> EEq (Eq ({lhs;rhs},EqP))
    | ((S _) as lhs), (Z as rhs) -> EGt (Gt ({lhs;rhs}, Gt0))
    | ((S sub_lhs) as lhs), ((S sub_rhs) as rhs) ->
      begin
        match cmp sub_lhs sub_rhs with
        | ELt Lt (_,lt_proof) -> ELt (Lt ({lhs;rhs}, LtS lt_proof))
        | EEq Eq _ -> EEq (Eq ({lhs;rhs},EqP))
        | EGt Gt (_,gt_proof) -> EGt (Gt ({lhs;rhs}, GtS gt_proof))
      end

  module Derived =
  struct

    let rec cmp_with_lt: type a b. a s t -> b t -> (a,b) lt_proof -> ((a s,b) lt, (a s,b) eq) Either.t =
      fun lhs rhs preproof ->
        match lhs,rhs,preproof with
        | (S Z as lhs),(S Z as rhs), Lt0 -> Right (Eq ({lhs;rhs},EqP))
        | (S Z as lhs),(S S _ as rhs), Lt0 -> Left (Lt ({lhs;rhs},LtS Lt0))
        | (S (S _sub_sub_lhs as sub_lhs) as lhs), (S (S _ as sub_rhs) as rhs), LtS sub_lt_proof ->
          begin
            match cmp_with_lt sub_lhs sub_rhs sub_lt_proof with
            | Left Lt (_,sub_lt_proof) -> Left (Lt ({lhs;rhs},LtS sub_lt_proof))
            | Right Eq _ -> Right (Eq ({lhs;rhs},EqP))
          end
        | S S _, S Z, LtS _ -> .
        | S _,Z,_ -> .
  end

end

module Add =
struct

  type (_,_,_) add = Add: ('a,'b,'c) add_data * ('a,'b,'c) add_proof -> ('a,'b,'c) add

  and ('lhs,'rhs,'sum) add_data = {lhs: 'lhs t; rhs: 'rhs t; sum: 'sum t}

  and (_,_,_) add_proof =
    | Add0: ('a,z,'a) add_proof
    | AddS: ('a s, 'b2, 'c) add_proof -> ('a, ('b2 s) as 'b, 'c) add_proof

  type (_,_) eadd =
    | EAdd: ('a,'b,'c) add -> ('a,'b) eadd

  let rec add: type a b. a t -> b t -> (a,b) eadd =
    fun lhs rhs ->
    match rhs with
    | Z as rhs -> EAdd (Add({lhs;rhs;sum=lhs}, Add0))
    | (S sub_rhs) as rhs ->
      begin
        match add (S lhs) sub_rhs with
        | EAdd Add ({sum;_}, add_proof) ->
          EAdd (Add ({lhs;rhs;sum},AddS add_proof))
      end

end

module Sub =
struct

  type (_,_,_) sub = Sub: ('a,'b,'c) sub_data * ('a,'b,'c) sub_proof -> ('a,'b,'c) sub

  and ('minuend,'substrahend,'difference) sub_data =
    {
      minuend: 'minuend t;
      substrahend: 'substrahend t;
      difference: 'difference t;
    }

  and (_,_,_) sub_proof =
    | Sub0: ('a,z,'a) sub_proof
    | SubS: ('a2,'b2,'c) sub_proof -> (('a2 s) as 'a,('b2 s) as 'b,'c) sub_proof

  type (_,_) esub =
    | ESub: ('a,'b,'c) sub -> ('a,'b) esub

  type (_,_) nesub =
    | NESub0: z t * (('a2 s) as 'a) t -> (z,'a) nesub
    | NESubS: (('a2 s) as 'a) t * (('b2 s) as 'b) t * ('a2,'b2) nesub -> ('a,'b) nesub

  let rec sub: type a b. a t -> b t -> ((a,b) esub, (a,b) nesub) Result.t =
    fun minuend substrahend ->
    match minuend,substrahend with
    | (Z as minuend),(S _ as substrahend) -> Error (NESub0 (minuend,substrahend))
    | minuend,(Z as substrahend) ->
      let difference = minuend in
      Ok (ESub (Sub ({minuend;substrahend;difference},Sub0)))
    | (S subminuend as minuend),(S subsubstrahend as substrahend) ->
      begin
        match sub subminuend subsubstrahend with
        | Ok ESub Sub ({difference;_},sub_proof) ->
          Ok (ESub (Sub ({minuend;substrahend;difference},SubS sub_proof)))
        | Error proof -> Error (NESubS (minuend,substrahend,proof))
      end

end

module Mul =
struct

  type (_,_,_) mul = Mul: ('a,'b,'c) mul_data * ('a,'b,'c) mul_proof -> ('a,'b,'c) mul

  and ('lhs,'rhs,'prod) mul_data = {lhs: 'lhs t; rhs: 'rhs t; prod: 'prod t}

  and (_,_,_) mul_proof =
    | Mul0: ('a,z,z) mul_proof
    | MulS: ('a,'b2,'d) mul_proof * ('a,'d,'c) Add.add_proof -> ('a,('b2 s) as 'b,'c) mul_proof

  type (_,_) emul =
    | EMul: ('a,'b,'c) mul -> ('a,'b) emul

  let rec mul: type a b. a t -> b t -> (a,b) emul =
    fun lhs rhs ->
    match rhs with
    | Z as rhs -> EMul (Mul ({lhs;rhs;prod=Z},Mul0))
    | (S sub_rhs) as _rhs ->
      begin
        match mul lhs sub_rhs with
        | EMul Mul ({lhs=_sub_lhs;rhs=_sub_rhs;prod=sub_prod},mul_proof) ->
          begin
            match Add.add lhs sub_prod with
            | EAdd Add ({sum=prod;_},add_proof) ->
              EMul (Mul ({lhs;rhs;prod},MulS (mul_proof,add_proof)))
          end
      end

end

module Div =
struct

  type ('a,'b,'c,'d) div_data = {dividend: 'a t; divisor: 'b t; quotient: 'c t; remainder: 'd t}

  and (_,_,_,_) div_proof =
    | DivP0: (z, _ s, z, z) div_proof
    | DivPR: ((('d2 s) as 'd),'b) Cmp.lt_proof * ('a2,((_ s) as 'b),'c,'d2) div_proof -> (('a2 s) as 'a,'b,'c,'d) div_proof
    | DivPQ: ('d2 s,'b) Cmp.eq_proof * ('a2,((_ s) as 'b),'c2,'d2) div_proof -> (('a2 s) as 'a,'b,(('c2 s) as 'c),z) div_proof

  and (_,_,_,_) div =
    | Div: ('a,'b,'c,'d) div_data * ('a,'b,'c,'d) div_proof -> ('a,'b,'c,'d) div

  type (_,_) ediv =
    | EDiv: ('a,'b,'c,'d) div -> ('a,'b) ediv

  type (_,_) nediv =
    | NEDiv0: 'a t * z t -> ('a,z) nediv

  type (_,_) clean_div = CDiv: ('a,'b,'c,z) div -> ('a,'b) clean_div

  let rec div: type a b. a t -> b t -> ((a,b) ediv,(a,b) nediv) Result.t =
    fun dividend divisor ->
    match dividend,divisor with
    | _,(Z as divisor) -> Error (NEDiv0 (dividend,divisor))
    | (Z as dividend),(S _ as divisor) ->
      let quotient = Z in
      let remainder = Z in
      Ok (EDiv (Div ({dividend;divisor;quotient;remainder},DivP0)))
    | (S sub_dividend as dividend),(S _ as divisor) ->
      begin
        match div sub_dividend divisor with
        | Ok EDiv Div ({dividend=Z;divisor=S Z as divisor;quotient=Z;remainder=Z},(DivP0 as sub_proof)) ->
          let dividend = S Z in
          let quotient = S Z in
          let remainder = Z in
          let proof = DivPQ (EqP, sub_proof) in
          Ok (EDiv (Div ({dividend;divisor;quotient;remainder},proof)))
        | Ok EDiv Div ({dividend=Z;divisor=S S _ as divisor;quotient=Z;remainder=Z},(DivP0 as sub_proof)) ->
          let dividend = S Z in
          let quotient = Z in
          let remainder = S Z in
          let proof = DivPR (LtS Lt0,sub_proof) in
          Ok (EDiv (Div ({dividend;divisor;quotient;remainder}, proof)))
        | Ok EDiv Div ({dividend=_;divisor=S S _ as divisor;
                        quotient=sub_quotient;remainder=S _ as sub_remainder},
                       (DivPR (lt_proof,_) as sub_proof))
          ->
          begin
            match Cmp.Derived.cmp_with_lt (S sub_remainder) divisor lt_proof with
            | Left Lt (_,(_ as proof)) ->
              let quotient = sub_quotient in
              let remainder = S sub_remainder in
              Ok (EDiv (Div ({dividend;divisor;quotient;remainder},DivPR (proof,sub_proof))))
            | Right Eq (_,_) ->
              let quotient = S sub_quotient in
              let remainder = Z in
              Ok (EDiv (Div ({dividend;divisor;quotient;remainder},DivPQ (EqP,sub_proof))))
          end
        | Ok EDiv Div ({dividend=_;divisor=S Z as divisor;
                        quotient=S _ as sub_quotient;remainder=Z},
                       (DivPQ (_eq_proof,_) as sub_proof)) ->
          let quotient = S sub_quotient in
          let remainder = Z in
          Ok (EDiv (Div ({dividend;divisor;quotient;remainder}, DivPQ (EqP, sub_proof))))
        | Ok EDiv Div ({dividend=_;divisor=S S _;
                        quotient=S _ as sub_quotient;remainder=Z},
                       (DivPQ (_eq_proof,_) as sub_proof)) ->
          let quotient = sub_quotient in
          let remainder = S Z in
          let lt_proof = Cmp.LtS Lt0 in
          Ok (EDiv (Div ({dividend;divisor;quotient;remainder}, DivPR (lt_proof,sub_proof))))
        | Ok EDiv Div ({divisor=S Z;remainder=S _;_},DivPR (LtS _,_)) -> .
        | Ok EDiv Div ({dividend=S _;divisor=S _;quotient=Z;_},_) -> .
        | Error _ -> .
      end

  module Derived =
  struct

    let tran_div: type a b c d k. (a,b,c,z) div -> (b,k,d,z) div -> (a,d) clean_div =
      fun first_div sub_div ->
      match first_div,sub_div with
      | Div ({dividend=Z as dividend;_},DivP0), Div ({quotient=divisor;_}, DivPQ _) ->
        let quotient = Z in
        let remainder = Z in
        CDiv (Div ({dividend;divisor;quotient;remainder},DivP0))
      | Div ({dividend=S _ as dividend;divisor=_;quotient=_;remainder=Z},DivPQ _),
        Div ({quotient= S _ as divisor;remainder=Z;_},DivPQ _)
        ->
        begin
          match div dividend divisor with
          | Ok EDiv (Div (_,DivPQ _) as div_proof) -> CDiv div_proof
          | Ok EDiv Div (_,DivPR _) -> failwith ""
          | Error _ -> .
        end
      | Div ({dividend=S _;remainder=Z;_},DivPQ _), Div ({quotient=Z;_},_) -> .

  end
end
